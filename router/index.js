const router = require("express").Router()
const home = require("./home")

// .. definisi router
router.use("/", home)

module.exports = router
const express = require("express")
const app = express()
const { PORT = 8080 } = process.env

// app.use(express.urlencoded({extended: false}))
app.use(express.json())

// ... import router
const router = require("./router")
app.use(router)

app.listen(PORT, () => {
    console.log(`server running on port ${PORT}`)
})
const router = require("express").Router()
const home = require("../controllers/homeController")

// .. definisi router
router.get("/", home.index)

module.exports = router